package view.user;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import main.StartFrame;
import model.User;
import service.UserService;

public class SignupUserFrame extends javax.swing.JFrame {

        UserService userService;
        User user;

        public SignupUserFrame() {
                initComponents();
                userService = new UserService();
                user = new User();
        }

        @SuppressWarnings("unchecked")
        // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
        private void initComponents() {

                jPanel1 = new javax.swing.JPanel();
                passwordField = new javax.swing.JPasswordField();
                signupButton = new javax.swing.JButton();
                yournameTextField = new javax.swing.JTextField();
                usernameTextField = new javax.swing.JTextField();
                backButton = new javax.swing.JButton();
                jScrollPane1 = new javax.swing.JScrollPane();
                aboutTextArea = new javax.swing.JTextArea();
                jLabel1 = new javax.swing.JLabel();
                jLabel4 = new javax.swing.JLabel();
                phoneTextField = new javax.swing.JTextField();
                jPanel2 = new javax.swing.JPanel();
                jLabel5 = new javax.swing.JLabel();
                jLabel6 = new javax.swing.JLabel();
                jLabel7 = new javax.swing.JLabel();
                jLabel8 = new javax.swing.JLabel();

                setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
                setPreferredSize(new java.awt.Dimension(865, 595));

                jPanel1.setBackground(new java.awt.Color(255, 153, 153));
                jPanel1.setPreferredSize(new java.awt.Dimension(865, 595));

                passwordField.setBackground(new java.awt.Color(255, 255, 255));
                passwordField.setFont(new java.awt.Font("Tw Cen MT", 1, 14)); // NOI18N
                passwordField.setForeground(new java.awt.Color(42, 59, 77));
                passwordField.setText("password");
                passwordField.setPreferredSize(new java.awt.Dimension(71, 22));
                passwordField.addFocusListener(new java.awt.event.FocusAdapter() {
                        public void focusGained(java.awt.event.FocusEvent evt) {
                                passwordFieldFocusGained(evt);
                        }
                        public void focusLost(java.awt.event.FocusEvent evt) {
                                passwordFieldFocusLost(evt);
                        }
                });
                passwordField.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                passwordFieldActionPerformed(evt);
                        }
                });

                signupButton.setBackground(new java.awt.Color(255, 51, 51));
                signupButton.setFont(new java.awt.Font("Tw Cen MT", 1, 18)); // NOI18N
                signupButton.setText("Sign up");
                signupButton.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                signupButtonActionPerformed(evt);
                        }
                });

                yournameTextField.setBackground(new java.awt.Color(255, 255, 255));
                yournameTextField.setFont(new java.awt.Font("Tw Cen MT", 1, 14)); // NOI18N
                yournameTextField.setForeground(new java.awt.Color(42, 59, 77));
                yournameTextField.setText("your name");
                yournameTextField.setToolTipText("");
                yournameTextField.addFocusListener(new java.awt.event.FocusAdapter() {
                        public void focusGained(java.awt.event.FocusEvent evt) {
                                yournameTextFieldFocusGained(evt);
                        }
                        public void focusLost(java.awt.event.FocusEvent evt) {
                                yournameTextFieldFocusLost(evt);
                        }
                });
                yournameTextField.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                yournameTextFieldActionPerformed(evt);
                        }
                });

                usernameTextField.setBackground(new java.awt.Color(255, 255, 255));
                usernameTextField.setFont(new java.awt.Font("Tw Cen MT", 1, 14)); // NOI18N
                usernameTextField.setForeground(new java.awt.Color(42, 59, 77));
                usernameTextField.setText("username");
                usernameTextField.setToolTipText("");
                usernameTextField.setPreferredSize(new java.awt.Dimension(71, 22));
                usernameTextField.addFocusListener(new java.awt.event.FocusAdapter() {
                        public void focusGained(java.awt.event.FocusEvent evt) {
                                usernameTextFieldFocusGained(evt);
                        }
                        public void focusLost(java.awt.event.FocusEvent evt) {
                                usernameTextFieldFocusLost(evt);
                        }
                });
                usernameTextField.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                usernameTextFieldActionPerformed(evt);
                        }
                });

                backButton.setBackground(new java.awt.Color(27, 43, 58));
                backButton.setFont(new java.awt.Font("Tw Cen MT", 1, 14)); // NOI18N
                backButton.setForeground(new java.awt.Color(204, 204, 204));
                backButton.setText("Back");
                backButton.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                backButtonActionPerformed(evt);
                        }
                });

                aboutTextArea.setBackground(new java.awt.Color(255, 255, 255));
                aboutTextArea.setColumns(20);
                aboutTextArea.setFont(new java.awt.Font("Tw Cen MT", 1, 14)); // NOI18N
                aboutTextArea.setForeground(new java.awt.Color(42, 59, 77));
                aboutTextArea.setRows(5);
                aboutTextArea.setText("about");
                aboutTextArea.addFocusListener(new java.awt.event.FocusAdapter() {
                        public void focusGained(java.awt.event.FocusEvent evt) {
                                aboutTextAreaFocusGained(evt);
                        }
                        public void focusLost(java.awt.event.FocusEvent evt) {
                                aboutTextAreaFocusLost(evt);
                        }
                });
                jScrollPane1.setViewportView(aboutTextArea);

                jLabel1.setText("*");

                jLabel4.setText("*");

                phoneTextField.setBackground(new java.awt.Color(255, 255, 255));
                phoneTextField.setFont(new java.awt.Font("Tw Cen MT", 1, 14)); // NOI18N
                phoneTextField.setForeground(new java.awt.Color(42, 59, 77));
                phoneTextField.setText("phone number");
                phoneTextField.addFocusListener(new java.awt.event.FocusAdapter() {
                        public void focusGained(java.awt.event.FocusEvent evt) {
                                phoneTextFieldFocusGained(evt);
                        }
                        public void focusLost(java.awt.event.FocusEvent evt) {
                                phoneTextFieldFocusLost(evt);
                        }
                });
                phoneTextField.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                phoneTextFieldActionPerformed(evt);
                        }
                });

                jPanel2.setBackground(new java.awt.Color(255, 255, 255));

                jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/signupicon.png"))); // NOI18N
                jLabel5.setPreferredSize(new java.awt.Dimension(389, 389));

                jLabel6.setFont(new java.awt.Font("Tw Cen MT", 1, 48)); // NOI18N
                jLabel6.setForeground(new java.awt.Color(49, 186, 254));
                jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
                jLabel6.setText("SIGN UP");
                jLabel6.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);

                javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
                jPanel2.setLayout(jPanel2Layout);
                jPanel2Layout.setHorizontalGroup(
                        jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addContainerGap())
                        .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(30, 30, 30)
                                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(31, Short.MAX_VALUE))
                );
                jPanel2Layout.setVerticalGroup(
                        jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                                .addContainerGap(41, Short.MAX_VALUE)
                                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel6)
                                .addGap(93, 93, 93))
                );

                jLabel7.setBackground(new java.awt.Color(38, 51, 57));
                jLabel7.setFont(new java.awt.Font("Tw Cen MT", 1, 48)); // NOI18N
                jLabel7.setForeground(new java.awt.Color(42, 59, 77));
                jLabel7.setText("Hi!");

                jLabel8.setBackground(new java.awt.Color(38, 51, 57));
                jLabel8.setFont(new java.awt.Font("Tw Cen MT", 0, 24)); // NOI18N
                jLabel8.setForeground(new java.awt.Color(42, 59, 77));
                jLabel8.setText("Create your account");

                javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
                jPanel1.setLayout(jPanel1Layout);
                jPanel1Layout.setHorizontalGroup(
                        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(backButton)
                                                .addGap(170, 170, 170))
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                                .addGap(40, 40, 40)
                                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 8, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 8, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                                        .addComponent(signupButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 295, Short.MAX_VALUE)
                                                                        .addComponent(phoneTextField)
                                                                        .addComponent(yournameTextField)
                                                                        .addComponent(usernameTextField, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                        .addComponent(passwordField, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                                .addGap(101, 101, 101)
                                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addComponent(jLabel8)
                                                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                                                .addGap(71, 71, 71)
                                                                                .addComponent(jLabel7)
                                                                                .addGap(73, 73, 73)))))
                                                .addContainerGap(66, Short.MAX_VALUE))))
                );
                jPanel1Layout.setVerticalGroup(
                        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGap(11, 11, 11)
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel8)
                                .addGap(12, 12, 12)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(usernameTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel1))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel4)
                                        .addComponent(passwordField, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(yournameTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(phoneTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(signupButton, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(backButton)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                );

                javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
                getContentPane().setLayout(layout);
                layout.setHorizontalGroup(
                        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                );
                layout.setVerticalGroup(
                        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                );

                pack();
                setLocationRelativeTo(null);
        }// </editor-fold>//GEN-END:initComponents

        private void passwordFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_passwordFieldActionPerformed
                // TODO add your handling code here:
        }//GEN-LAST:event_passwordFieldActionPerformed

        private void signupButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_signupButtonActionPerformed
                // TODO add your handling code here:
                if ((!"".equals(usernameTextField.getText()) && !"your name".equals(usernameTextField.getText())) && (!"".equals(passwordField.getText()) && !"password".equals(passwordField.getText()))) {
                        try {
                                user.setName(yournameTextField.getText());
                                user.setPhone(phoneTextField.getText());
                                user.setUsername(usernameTextField.getText());
                                user.setPassword(String.valueOf(passwordField.getPassword()));
                                user.setAbout(aboutTextArea.getText());
                                userService.addUser(user);
                        } catch (SQLException ex) {
                                Logger.getLogger(SignupUserFrame.class.getName()).log(Level.SEVERE, null, ex);
                        }

                        this.dispose();
                        new StartFrame().setVisible(true);
                } else {
                        JOptionPane.showMessageDialog(this, "Enter your Username and Password", "Missing", JOptionPane.ERROR_MESSAGE);
                }
        }//GEN-LAST:event_signupButtonActionPerformed

        private void yournameTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_yournameTextFieldActionPerformed
                // TODO add your handling code here:
        }//GEN-LAST:event_yournameTextFieldActionPerformed

        private void usernameTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_usernameTextFieldActionPerformed
                // TODO add your handling code here:
        }//GEN-LAST:event_usernameTextFieldActionPerformed

        private void backButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backButtonActionPerformed
                // TODO add your handling code here:
                this.dispose();
                new StartFrame().setVisible(true);
        }//GEN-LAST:event_backButtonActionPerformed

        private void usernameTextFieldFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_usernameTextFieldFocusGained
                // TODO add your handling code here:
                if (usernameTextField.getText().equals("username")) {
                        usernameTextField.setText("");
                }
        }//GEN-LAST:event_usernameTextFieldFocusGained

        private void usernameTextFieldFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_usernameTextFieldFocusLost
                // TODO add your handling code here
                if (usernameTextField.getText().equals("")) {
                        usernameTextField.setText("username");
                }
        }//GEN-LAST:event_usernameTextFieldFocusLost

        private void passwordFieldFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_passwordFieldFocusGained
                // TODO add your handling code here:
                if (passwordField.getText().equals("password")) {
                        passwordField.setText("");
                }
        }//GEN-LAST:event_passwordFieldFocusGained

        private void passwordFieldFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_passwordFieldFocusLost
                // TODO add your handling code here:
                if (passwordField.getText().equals("")) {
                        passwordField.setText("password");
                }
        }//GEN-LAST:event_passwordFieldFocusLost

        private void yournameTextFieldFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_yournameTextFieldFocusGained
                // TODO add your handling code here:
                if (yournameTextField.getText().equals("your name")) {
                        yournameTextField.setText("");
                }
        }//GEN-LAST:event_yournameTextFieldFocusGained

        private void yournameTextFieldFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_yournameTextFieldFocusLost
                // TODO add your handling code here:
                if (yournameTextField.getText().equals("")) {
                        yournameTextField.setText("your name");
                }
        }//GEN-LAST:event_yournameTextFieldFocusLost

        private void aboutTextAreaFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_aboutTextAreaFocusGained
                // TODO add your handling code here:
                if (aboutTextArea.getText().equals("about")) {
                        aboutTextArea.setText("");
                }
        }//GEN-LAST:event_aboutTextAreaFocusGained

        private void aboutTextAreaFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_aboutTextAreaFocusLost
                // TODO add your handling code here:
                if (aboutTextArea.getText().equals("")) {
                        aboutTextArea.setText("about");
                }
        }//GEN-LAST:event_aboutTextAreaFocusLost

        private void phoneTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_phoneTextFieldActionPerformed
                // TODO add your handling code here:
        }//GEN-LAST:event_phoneTextFieldActionPerformed

        private void phoneTextFieldFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_phoneTextFieldFocusGained
                // TODO add your handling code here:
                if (phoneTextField.getText().equals("phone number")) {
                        phoneTextField.setText("");
                }
        }//GEN-LAST:event_phoneTextFieldFocusGained

        private void phoneTextFieldFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_phoneTextFieldFocusLost
                // TODO add your handling code here:
                if (phoneTextField.getText().equals("")) {
                        phoneTextField.setText("phone number");
                }
        }//GEN-LAST:event_phoneTextFieldFocusLost

        /**
         * @param args the command line arguments
         */

        // Variables declaration - do not modify//GEN-BEGIN:variables
        private javax.swing.JTextArea aboutTextArea;
        private javax.swing.JButton backButton;
        private javax.swing.JLabel jLabel1;
        private javax.swing.JLabel jLabel4;
        private javax.swing.JLabel jLabel5;
        private javax.swing.JLabel jLabel6;
        private javax.swing.JLabel jLabel7;
        private javax.swing.JLabel jLabel8;
        private javax.swing.JPanel jPanel1;
        private javax.swing.JPanel jPanel2;
        private javax.swing.JScrollPane jScrollPane1;
        private javax.swing.JPasswordField passwordField;
        private javax.swing.JTextField phoneTextField;
        private javax.swing.JButton signupButton;
        private javax.swing.JTextField usernameTextField;
        private javax.swing.JTextField yournameTextField;
        // End of variables declaration//GEN-END:variables
}
