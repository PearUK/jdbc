package view.user;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.User;
import service.UserService;

public class AddUserFrame extends javax.swing.JFrame {

        UserService userService;
        User user;

        public AddUserFrame() {
                initComponents();
                userService = new UserService();
                user = new User();
        }

        @SuppressWarnings("unchecked")
        // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
        private void initComponents() {

                buttonGroup1 = new javax.swing.ButtonGroup();
                jPanel1 = new javax.swing.JPanel();
                jLabel1 = new javax.swing.JLabel();
                jLabel2 = new javax.swing.JLabel();
                nameTextField = new javax.swing.JTextField();
                jLabel3 = new javax.swing.JLabel();
                phoneTextField = new javax.swing.JTextField();
                jLabel4 = new javax.swing.JLabel();
                userNameTextField = new javax.swing.JTextField();
                jLabel5 = new javax.swing.JLabel();
                passwordField = new javax.swing.JPasswordField();
                jLabel7 = new javax.swing.JLabel();
                jScrollPane1 = new javax.swing.JScrollPane();
                aboutTextArea = new javax.swing.JTextArea();
                jLabel8 = new javax.swing.JLabel();
                adminRadioButton = new javax.swing.JRadioButton();
                userRadioButton = new javax.swing.JRadioButton();
                backButton = new javax.swing.JButton();
                confirmButton = new javax.swing.JButton();

                setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

                jLabel1.setBackground(new java.awt.Color(102, 153, 0));
                jLabel1.setFont(new java.awt.Font("Tw Cen MT", 1, 48)); // NOI18N
                jLabel1.setForeground(new java.awt.Color(102, 153, 0));
                jLabel1.setText("Add User");

                jLabel2.setBackground(new java.awt.Color(102, 153, 0));
                jLabel2.setFont(new java.awt.Font("Tw Cen MT", 1, 14)); // NOI18N
                jLabel2.setForeground(new java.awt.Color(102, 153, 0));
                jLabel2.setText("Name");

                nameTextField.setFont(new java.awt.Font("Tw Cen MT", 1, 14)); // NOI18N
                nameTextField.setForeground(new java.awt.Color(102, 153, 0));
                nameTextField.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                nameTextFieldActionPerformed(evt);
                        }
                });

                jLabel3.setFont(new java.awt.Font("Tw Cen MT", 1, 14)); // NOI18N
                jLabel3.setForeground(new java.awt.Color(102, 153, 0));
                jLabel3.setText("Phone");

                phoneTextField.setFont(new java.awt.Font("Tw Cen MT", 1, 14)); // NOI18N
                phoneTextField.setForeground(new java.awt.Color(102, 153, 0));
                phoneTextField.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                phoneTextFieldActionPerformed(evt);
                        }
                });

                jLabel4.setFont(new java.awt.Font("Tw Cen MT", 1, 14)); // NOI18N
                jLabel4.setForeground(new java.awt.Color(102, 153, 0));
                jLabel4.setText("User Name");

                userNameTextField.setFont(new java.awt.Font("Tw Cen MT", 1, 14)); // NOI18N
                userNameTextField.setForeground(new java.awt.Color(102, 153, 0));
                userNameTextField.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                userNameTextFieldActionPerformed(evt);
                        }
                });

                jLabel5.setFont(new java.awt.Font("Tw Cen MT", 1, 14)); // NOI18N
                jLabel5.setForeground(new java.awt.Color(102, 153, 0));
                jLabel5.setText("Password");

                passwordField.setFont(new java.awt.Font("Tw Cen MT", 1, 14)); // NOI18N
                passwordField.setForeground(new java.awt.Color(102, 153, 0));

                jLabel7.setFont(new java.awt.Font("Tw Cen MT", 1, 14)); // NOI18N
                jLabel7.setForeground(new java.awt.Color(102, 153, 0));
                jLabel7.setText("About");

                aboutTextArea.setColumns(20);
                aboutTextArea.setFont(new java.awt.Font("Tw Cen MT", 1, 14)); // NOI18N
                aboutTextArea.setForeground(new java.awt.Color(102, 153, 0));
                aboutTextArea.setRows(5);
                jScrollPane1.setViewportView(aboutTextArea);

                jLabel8.setFont(new java.awt.Font("Tw Cen MT", 1, 14)); // NOI18N
                jLabel8.setForeground(new java.awt.Color(102, 153, 0));
                jLabel8.setText("Vai Tro");

                buttonGroup1.add(adminRadioButton);
                adminRadioButton.setFont(new java.awt.Font("Tw Cen MT", 1, 14)); // NOI18N
                adminRadioButton.setForeground(new java.awt.Color(102, 153, 0));
                adminRadioButton.setText("Admin");

                buttonGroup1.add(userRadioButton);
                userRadioButton.setFont(new java.awt.Font("Tw Cen MT", 1, 14)); // NOI18N
                userRadioButton.setForeground(new java.awt.Color(102, 153, 0));
                userRadioButton.setSelected(true);
                userRadioButton.setText("User");

                backButton.setBackground(new java.awt.Color(102, 153, 0));
                backButton.setFont(new java.awt.Font("Tw Cen MT", 1, 18)); // NOI18N
                backButton.setText("Back");
                backButton.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                backButtonActionPerformed(evt);
                        }
                });

                confirmButton.setBackground(new java.awt.Color(102, 153, 0));
                confirmButton.setFont(new java.awt.Font("Tw Cen MT", 1, 18)); // NOI18N
                confirmButton.setText("Confirm");
                confirmButton.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                confirmButtonActionPerformed(evt);
                        }
                });

                javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
                jPanel1.setLayout(jPanel1Layout);
                jPanel1Layout.setHorizontalGroup(
                        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(17, 17, 17)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabel8)
                                        .addComponent(jLabel7)
                                        .addComponent(jLabel5)
                                        .addComponent(jLabel4)
                                        .addComponent(jLabel2)
                                        .addComponent(jLabel3))
                                .addGap(49, 49, 49)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel1)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                .addGroup(jPanel1Layout.createSequentialGroup()
                                                        .addComponent(confirmButton, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(backButton, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(passwordField, javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(userNameTextField, javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(phoneTextField, javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(nameTextField, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                                        .addComponent(adminRadioButton)
                                                        .addGap(18, 18, 18)
                                                        .addComponent(userRadioButton))))
                                .addContainerGap(42, Short.MAX_VALUE))
                );
                jPanel1Layout.setVerticalGroup(
                        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel1)
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel2)
                                        .addComponent(nameTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel3)
                                        .addComponent(phoneTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel4)
                                        .addComponent(userNameTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(passwordField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel5))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel7))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(adminRadioButton)
                                        .addComponent(jLabel8)
                                        .addComponent(userRadioButton))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(confirmButton)
                                        .addComponent(backButton))
                                .addContainerGap(56, Short.MAX_VALUE))
                );

                javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
                getContentPane().setLayout(layout);
                layout.setHorizontalGroup(
                        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                );
                layout.setVerticalGroup(
                        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                );

                pack();
                setLocationRelativeTo(null);
        }// </editor-fold>//GEN-END:initComponents

    private void nameTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nameTextFieldActionPerformed
            // TODO add your handling code here:
    }//GEN-LAST:event_nameTextFieldActionPerformed

    private void phoneTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_phoneTextFieldActionPerformed
            // TODO add your handling code here:
    }//GEN-LAST:event_phoneTextFieldActionPerformed

    private void userNameTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_userNameTextFieldActionPerformed
            // TODO add your handling code here:
    }//GEN-LAST:event_userNameTextFieldActionPerformed

    private void backButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backButtonActionPerformed
            try {
                    // TODO add your handling code here:
                    new ListUserFrame().setVisible(true);
            } catch (SQLException ex) {
                    Logger.getLogger(AddUserFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
            this.dispose();
    }//GEN-LAST:event_backButtonActionPerformed

    private void confirmButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_confirmButtonActionPerformed
            user.setName(nameTextField.getText());
            user.setPhone(phoneTextField.getText());
            user.setUsername(userNameTextField.getText());
            user.setPassword(String.valueOf(passwordField.getPassword()));
            user.setAbout(aboutTextArea.getText());

            String role = "ROLE_USER";
            if (adminRadioButton.isSelected()) {
                    role = "ROLE_ADMIN";
            }
            if (userRadioButton.isSelected()) {
                    role = "ROLE_USER";
            }
            user.setRole(role);

            try {
                    userService.addUser(user);
                    new ListUserFrame().setVisible(true);
            } catch (SQLException ex) {
                    Logger.getLogger(AddUserFrame.class.getName()).log(Level.SEVERE, null, ex);
            }

            this.dispose();
    }//GEN-LAST:event_confirmButtonActionPerformed

        /**
         * @param args the command line arguments
         */

        // Variables declaration - do not modify//GEN-BEGIN:variables
        private javax.swing.JTextArea aboutTextArea;
        private javax.swing.JRadioButton adminRadioButton;
        private javax.swing.JButton backButton;
        private javax.swing.ButtonGroup buttonGroup1;
        private javax.swing.JButton confirmButton;
        private javax.swing.JLabel jLabel1;
        private javax.swing.JLabel jLabel2;
        private javax.swing.JLabel jLabel3;
        private javax.swing.JLabel jLabel4;
        private javax.swing.JLabel jLabel5;
        private javax.swing.JLabel jLabel7;
        private javax.swing.JLabel jLabel8;
        private javax.swing.JPanel jPanel1;
        private javax.swing.JScrollPane jScrollPane1;
        private javax.swing.JTextField nameTextField;
        private javax.swing.JPasswordField passwordField;
        private javax.swing.JTextField phoneTextField;
        private javax.swing.JTextField userNameTextField;
        private javax.swing.JRadioButton userRadioButton;
        // End of variables declaration//GEN-END:variables
}
